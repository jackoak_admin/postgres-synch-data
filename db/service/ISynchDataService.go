package service

import (
	"gorm.io/gorm"
	"synch-data/db/conf"
	"synch-data/db/mapper"
	"synch-data/models"
)

func QueryDataMaster(leaderConf conf.LeaderConfig) []models.SynchData  {
	return mapper.QueryDataMaster(leaderConf)
}

func DeleteMaster(data []models.SynchData,db *gorm.DB)*gorm.DB {
	return mapper.DeleteMaster(data,db)
}



func InsertSlaver(data []models.SynchData,db *gorm.DB)*gorm.DB  {
	return mapper.InsertSlaver(data,db)
}

