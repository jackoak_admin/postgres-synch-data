package mapper

import (
	"gorm.io/gorm"
	"synch-data/db/conf"
	"synch-data/models"
)

func  QueryDataMaster(leaderConf conf.LeaderConfig) []models.SynchData  {
	channelId := leaderConf.ChannelId
	limit := leaderConf.Limit
	var data []models.SynchData
	sql := "select * from synch.synch_data where 1=1 and channel_id= ? order by create_time asc limit ?"
	leaderConf.GetMasterCon().Raw(sql,channelId,limit).Scan(&data)
	return data
}

func DeleteMaster(data []models.SynchData,db *gorm.DB)*gorm.DB{
	return db.Debug().Table("synch.synch_data").Delete(&data)
}

func InsertSlaver(data []models.SynchData,db *gorm.DB) *gorm.DB  {
	return  db.Debug().Table("synch.synch_data").Create(&data)
}

