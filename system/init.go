package system

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"sync"
	"synch-data/db/conf"
	"synch-data/job"
	_ "synch-data/routers"
)

var lock sync.Mutex






var confList []conf.LeaderConfig

func init()  {
	confArray,b := conf.LoadDbConfig()
	if !b{
		panic("load db config fail...")
	}
	confList = confArray
	fmt.Printf("config info--> %v ", confArray)
}


func Start() {

	for _,dbItem := range confList{
		crontab := cron.New(cron.WithSeconds())
		crontab.AddFunc(dbItem.Spec, doJob)
		crontab.Start()
	}


}




func doJob(){
	fmt.Println("doing Job...")
	lock.Lock()
	defer lock.Unlock()


	for _,dbItem := range confList{
		job.SynchData(dbItem)
	}

}

