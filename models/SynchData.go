package models




import (
	"github.com/jinzhu/gorm/dialects/postgres"
	"time"
)





type SynchData struct {
	Id 			string 					`gorm:"column:id"`
	TabSchema   string					`gorm:"column:tab_schema"`
	TabName		string					`gorm:"column:tab_name"`
	Opt			string					`gorm:"column:opt"`
	ChannelId	string					`gorm:"column:channel_id"`
	DataNew		postgres.Jsonb			`gorm:"column:data_new"`
	DataOld		postgres.Jsonb			`gorm:"column:data_old"`
	CreateTime time.Time				`gorm:"column:create_time"`
	TableId	int64						`gorm:"column:table_id"`

}

