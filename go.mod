module synch-data

go 1.15

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/robfig/cron/v3 v3.0.1
	github.com/smartystreets/goconvey v1.6.4
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/gorm v1.20.12
)
