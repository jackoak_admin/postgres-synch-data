package main

import (
	beego "github.com/beego/beego/v2/server/web"
	_ "synch-data/routers"
	"synch-data/system"
)



func init() {
	system.Start()
}


func main() {
	beego.Run()
}

