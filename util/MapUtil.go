package util


func GetValues(m map[string]interface{}) []interface{} {
	// 数组默认长度为map长度,后面append时,不需要重新申请内存和拷贝,效率较高
	keys := make([]interface{}, 0, len(m))
	for _,v := range m {
		keys = append(keys, v)
	}
	return keys
}


func GetKeys(m map[string]interface{}) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return keys
}