package main

import (
	"fmt"
)



func main() {

	numbers := []int{0,1,2,3,4,5,6,7,8}

	/* 打印原始切片 */
	fmt.Println("numbers ==", numbers)

	/* 打印子切片从索引1(包含) 到索引4(不包含)*/
	fmt.Println("numbers[1:4] ==", numbers[0:4])
	fmt.Println("numbers[4: %s ==",len(numbers)-1, numbers[4:8])
}


func printSlice(x []int){
	fmt.Printf("len=%d cap=%d slice=%v\n",len(x),cap(x),x)
}

func getArray(array []int)[]int  {

	arr := array[0:1]

	return arr

}